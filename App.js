import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import {connect} from 'react-redux';
import {updateItems} from "./src/store/actions";
import Item from "./src/components/Item/Item";


class App extends Component {
	componentDidMount() {
		this.props.updateItems();
	}
	render() {
		return (
			<View style={styles.container}>
				<FlatList
					data={this.props.items}
					renderItem={(info) => (
						<Item image={info.item.img} title={info.item.title}/>
					)}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'flex-start',
		padding: 10
	}
});

const mapStateToProps = state => {
	return {
		items: state.items
	}
};

const mapDispatchToProps = dispatch => {
	return {
		updateItems: () => dispatch(updateItems())
	}
};

export default connect(mapStateToProps, mapDispatchToProps)(App);