import React from 'react';
import {AppRegistry} from 'react-native';
import App from './App';

import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import reducer from './src/store/reducer';
import thunkMiddleware from 'redux-thunk'
import axios from "axios";

axios.defaults.baseURL = 'https://www.reddit.com/r';

const store = createStore(reducer,applyMiddleware(thunkMiddleware));

const Application = () => (
	<Provider store={store}>
		<App/>
	</Provider>
);

AppRegistry.registerComponent('reddit', () => Application);