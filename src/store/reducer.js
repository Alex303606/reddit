import {REDDIT_TODO_SUCCESS} from "./actionsTypes";

const initialState = {
	items: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case REDDIT_TODO_SUCCESS:
			return {
				...state, items: action.items.map(item => ({
					img: {uri: item.data.thumbnail},
					key: item.data.thumbnail,
					title: item.data.title
				}))
			};
		default:
			return state;
	}
};

export default reducer;