import axios from "axios/index";

import {REDDIT_TODO_ERROR, REDDIT_TODO_REQUEST, REDDIT_TODO_SUCCESS} from "./actionsTypes";

export const redditTodoRequest = () => {
	return {type: REDDIT_TODO_REQUEST};
};

export const redditTodoSuccess = (items) => {
	return {type: REDDIT_TODO_SUCCESS, items};
};

export const redditTodoError = (error) => {
	return {type: REDDIT_TODO_ERROR, error};
};

export const updateItems = () => {
	return dispatch => {
		dispatch(redditTodoRequest());
		axios.get('/pics.json').then(response => {
			if (response.data) dispatch(redditTodoSuccess(response.data.data.children));
		}, error => {
			dispatch(redditTodoError(error));
		});
	}
};