import React from 'react'
import {Image, StyleSheet, Text, View} from "react-native";

const Item = props => {
	return (
		<View elevation={3} style={styles.item}>
			<Image source={props.image} style={styles.image}/>
			<Text style={styles.text}>{props.title}</Text>
		</View>
	)
};

const styles = StyleSheet.create({
	item: {
		flexDirection: 'row',
		alignItems: 'center',
		width: '100%',
		backgroundColor: '#eee',
		marginBottom: 20,
		padding: 10
	},
	image: {
		width: '20%',
		height: 50
	},
	text: {
		paddingRight: 10,
		paddingLeft: 10,
		width: '80%'
	}
});

export default Item;